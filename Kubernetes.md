# Ablauf
## Zeigen
- Docker for Windows Kubernetes zeigen 
- Docker -> Settings -> Kubernetes -> Enable
- Alternativ minikube
    - Läuft auf eigener VM statt auf der MobyVM
- Kubeadm
    - Overkill. "Full Size K8s"

### Nodes
kubectl describe nodes              - Standardnode docker-desktop. Nur 1 Node konfiguriert
kubectl describe node docker-desktop
### Pods
kubectl get pods                    - Leer
kubectl apply -f 1-pod.yaml         - Erzeuge Pod
kubectl get pods                    - 1 Pod
kubectl logs                        - Loggs des Containers anzeigen
kubectl exec -it                    - sh in Container aufschalten
kubectl describe pod skyway             
(Browser: localhost:8080/sysinfo)

### ReplicaSets
kubectl apply -f 2-replicaset.yaml
kubectl get pods
kubectl delete pod skyway-webdemo       - Pod wird gelöscht. Kein neuer
kubectl get pods 
kubectl logs                            - Nochmal ins Log schauen
kubectl describe pod                    - Podbeschreibung -> IP
kubectl delete -f 2-replicaset.yaml

### Deployment
(Management von ReplicaSets -> Auslieferung neue Versionen)
kubectl apply -f 3-deployment.yaml
kubectl get pods                        - Podname wird länger
(Browser: localhost:8080/sysinfo)

### Service 
kubectl apply -f 4-deployment+service.yaml  - Zeigen, dass nur der Service geändert wird
kubectl get svc skyway-service
(Browser: localhost:8080/sysinfo)
kubectl get all
kubectl apply -f  5-deployment-another-image.yaml
kubectl get all
kubectl logs 
kubectl apply -f 4-deployment+service.yaml 
kubectl delete -f 4-deployment+service.yaml
